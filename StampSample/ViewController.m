//
//  ViewController.m
//  StampSample
//
//  Created by HARADASHINYA on 10/30/13.
//  Copyright (c) 2013 HARADASHINYA. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
{
    int tappedCount;
    UIImagePickerController *pickerLibrary ;
    
    //画像をハイライトするView
    UIView *hilightView;
    UIImageView *imgView;
    //現在存在するViewの名前(stampView,mainView);
    NSString *currentView;
    ImageCollection *imgCollection;
    BOOL isALreadyVisible;
    //デコレーションを入れる複数の画像
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.stampView setHidden:YES];
    [self.navigationController.navigationBar setHidden:YES];
    
    
    // 既に画像が表示がされているかどうか
    isALreadyVisible = NO;
    
    // 画像のドラッグ検出を可能にする。
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onDrag:)];
    [self.view addGestureRecognizer:pan];
    
    imgCollection = [ ImageCollection shared];
    [imgCollection setup];
    imgCollection.subViews = [[NSMutableArray alloc] init];
	// Do any additional setup after loading the view, typically from a nib.
}


-(void)addSelectedImg
{
    NSLog(@"新しく画像を挿入しました.");
    imgCollection.selectedImgView.tag = 10;
    CGRect tmpFrame = imgCollection.selectedImgView.frame;
    [imgCollection.selectedImgView setFrame:CGRectMake(tmpFrame.origin.x+30, tmpFrame.origin.y+30, 40, 40)];
    [self.view addSubview:imgCollection.selectedImgView];
    isALreadyVisible = YES;

    
}
-(void)onDrag:(id)sender
{
    // もしも画像が選択されていなかったり、stampViewが表示されていたら、ドラッグイベントを無効化する。
    if (([self.stampView isHidden] == NO) || imgCollection.selectedImgView == NULL){
        return;
    }
    
    // 画像がViewに表示されていなかったら、挿入する。
    if (isALreadyVisible == NO){
        [self addSelectedImg];
    }
    
    
    // ドラッグ実装の肝 79 ~ 90行目
    
    UIView *targetView = [self.view viewWithTag:10];
    
    // ドラッグしたあとのポイントを取得する。
    CGPoint  p = [sender translationInView:self.view];
    CGPoint movedPoint = CGPointMake(targetView.center.x + p.x, targetView.center.y+p.y);
    
    targetView.center = movedPoint;
    
    //累積したx,y座標を初期化する。
    [sender setTranslation:CGPointZero inView:self.view];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)onStamp:(id)sender {
    
    //StampViewが隠されていた場合はStampViewは表示させる。
    if ([self.stampView isHidden]){
        // 画像を表示させる。
        [self.stampView setHidden:NO];
        
        // スタンプ画面を表示させる。
        isALreadyVisible = NO;
        UIView *willRemovedView = [self.view viewWithTag:10];

        if (willRemovedView != NULL)
            [willRemovedView removeFromSuperview];
        
        int rowNum = ceil([imgCollection.fileNames count]/3.0);
        int colNum = 3;
        
        // グリッドで表示
        for(int i = 0; i < colNum;i++){
            for(int j = 0 ; j < rowNum;j++){
                int idx = i + 3*j;
                if (idx < [imgCollection.fileNames count]){
                    UIImage *img = [imgCollection.fileNames objectAtIndex:idx];
                    
                    UIImage *scaledImg = [UIImage imageWithCGImage:[img CGImage] scale:(img.scale * 2)
                                                       orientation:UIImageOrientationUp];
                    
                    // subViewsに対してサムネイル画像を挿入する。
                    UIImageView *subView = [[UIImageView alloc] initWithImage:scaledImg];
                    CGRect frame = CGRectMake(100*i + 30, j*50, 40, 40);
                    
                    [subView setFrame:frame];
                    [self.stampView addSubview:subView];
                    [subView setUserInteractionEnabled:YES];
                    [imgView setUserInteractionEnabled:YES];
                    [imgCollection.subViews addObject:subView];
                    [imgView setHidden:NO];
                }

            }
        };
        

    }else{
        [self hideStampView];

    }


}

//保存ボタンを押すと以下のメソッドが呼ばれる
- (IBAction)onCamera:(id)sender {
    UIGraphicsBeginImageContext(self.view.frame.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    // PNGの場合（view.alphaで指定した透明度も維持されるみたい）
    NSData *dataSaveImage = UIImagePNGRepresentation(image);
    
    
    // Documentsディレクトリに保存
    NSString *uuid = [[NSUUID UUID] UUIDString];


    NSString *path = [self buildPath];
    [dataSaveImage writeToFile:[path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",uuid]] atomically:YES];
    UIImage *savedImg = [[UIImage alloc] initWithData:dataSaveImage scale:1.0];
    
    
    // カメラアルバムに保存する
    SEL sel = @selector(onCompleteCapture:didFinishSavingWithError:contextInfo:);

    //画像をディスクに保存する
    UIImageWriteToSavedPhotosAlbum(savedImg,self,sel,NULL);
}

//onAddを押すと、保存した画像をUIImagePickerControllerで保存、取得できる。
- (IBAction)onAdd:(id)sender {
    pickerLibrary = [[UIImagePickerController alloc ] init];
    pickerLibrary.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerLibrary.delegate = self;
    [self presentViewController:pickerLibrary animated:YES completion:nil];

}
-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (![ud objectForKey:@"images"]){
        [ud setObject:[NSArray new] forKey:@"images"];
    }
    
    // 保存するときはNSArrayに変換してから保存する
    NSMutableArray *preData = [[ud objectForKey:@"images"] mutableCopy];
    
    // URLのパスデータを保存する
    NSString *url = [[info objectForKey:UIImagePickerControllerReferenceURL] description];
    NSLog(@"url is %@",url);
    NSArray *postData = [preData copy];
    
    [ud setObject:postData forKey:@"images"];
    [imgCollection addImageTofileNames:image];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [pickerLibrary dismissViewControllerAnimated:YES completion:nil];
    
}





- (void)onCompleteCapture:(UIImage *)screenImage
 didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{

    NSString *message = @"画像を保存しました";
    if (error) message = @"画像の保存に失敗しました";
    
    NSLog(@"画像を保存しました");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @""
                                                    message: message
                                                   delegate: nil
                                          cancelButtonTitle: @"OK"
                                          otherButtonTitles: nil];
    [alert show];
    
}


-(NSString *)buildPath{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    return path;
    
}
//画像のパスから、ImageViewを作成
-(UIImageView *)loadImgFrom:(NSString*)path
{
    NSData *d = [[NSData alloc] initWithData:[NSData dataWithContentsOfFile:path]];
    NSLog(@"d is %@",d);
    UIImage *img = [[UIImage alloc] initWithData:d scale:1];
    UIImageView *a = [[UIImageView alloc] initWithImage:img];
    
    return a;
}




-(void)hideStampView
{
    // スタンプViewが消える処理をする。
    [self.stampView setHidden:YES];
    [self addSelectedImg];
    // 現在のViewはmainViewになる。
    
    // hilightViewを削除する。
    [hilightView removeFromSuperview];
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //StampViewが隠されていた場合は、動作させない。
    NSLog(@"touche");
    if ([self.stampView isHidden]) return;
    


    //選択した画像をselectedImgViewに画像を格納する。
    if ([imgCollection.subViews count] > 0){
        for(int i = 0 ; i < [imgCollection.subViews count]; i++){
            UIImageView *view = [imgCollection.subViews objectAtIndex:i];
            
            //　タップした画像と指が交錯したらviewを返す。
            if ([event touchesForView:view]){
                
                // ハイライトされたViewがあれば消去する。
                [hilightView removeFromSuperview];
                // タッチした選択画像をimgCollection.selectedImgViewにセットする。
                imgCollection.selectedImgView = [imgCollection.subViews objectAtIndex:i];
                CGRect tmpFrame = CGRectMake(imgCollection.selectedImgView.frame.origin.x,
                                             imgCollection.selectedImgView.frame.origin.y +50+imgCollection.selectedImgView.frame.size.height/2,
                                             imgCollection.selectedImgView.frame.size.width,
                                             imgCollection.selectedImgView.frame.size.height);
                hilightView = [[UIView alloc] initWithFrame:tmpFrame];
                hilightView.layer.opacity = 0.8;
                [hilightView setBackgroundColor:[UIColor orangeColor]];
                [self.view addSubview:hilightView];
            }
        }
    }
    
}

@end
