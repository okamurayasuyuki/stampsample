//
//  ImgViewController.m
//  StampSample
//
//  Created by HARADASHINYA on 10/30/13.
//  Copyright (c) 2013 HARADASHINYA. All rights reserved.
//

#import "ImgViewController.h"

@interface ImgViewController ()

@end

@implementation ImgViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"hello");
    [self.view setBackgroundColor:[UIColor whiteColor]];

	// Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
