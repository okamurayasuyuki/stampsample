//
//  ImageCollection.h
//  StampSample
//
//  Created by HARADASHINYA on 10/30/13.
//  Copyright (c) 2013 HARADASHINYA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface ImageCollection : NSObject


//デコレーションの画像の名前を格納
// UIImages
@property NSMutableArray *fileNames;

//選択された画像を格納
@property UIImageView *selectedImgView;
@property NSMutableArray *subViews;

+(id)shared;
-(void)setup;
-(void)addImageTofileNames:(UIImage *)img;



@end
