//
//  ViewController.h
//  StampSample
//
//  Created by HARADASHINYA on 10/30/13.
//  Copyright (c) 2013 HARADASHINYA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImgViewController.h"
#import "ImageCollection.h"
#import <QuartzCore/QuartzCore.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface ViewController : UIViewController<UICollectionViewDelegateFlowLayout,UICollectionViewDelegateFlowLayout,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UIView *stampView;

@property (strong, nonatomic) IBOutlet UIButton *button;

- (IBAction)onStamp:(id)sender;
- (IBAction)onCamera:(id)sender;
- (IBAction)onAdd:(id)sender;

@end
