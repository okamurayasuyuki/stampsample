//
//  ImageCollection.m
//  StampSample
//
//  Created by HARADASHINYA on 10/30/13.
//  Copyright (c) 2013 HARADASHINYA. All rights reserved.
//

#import "ImageCollection.h"


@implementation ImageCollection
{

}
static ImageCollection *imageCollection;
+(id)shared
{
    if (imageCollection == NULL){
        imageCollection = [[ImageCollection alloc] init];
    }
    return imageCollection;
    
}

-(void)add:(ImageCollection *)object
{
    // 画像データを格納する
    [self.fileNames addObject:object];
}

-(void)addImageTofileNames:(UIImage *)img
{
    NSLog(@"called hello owrld");
    [self.fileNames addObject:img];
    NSLog(@"added");
}




-(void)setup
{
    //画像ファイル名をfileNamesのプロパティを格納する。
    // 左端が一番最初に表示される画像
    NSMutableArray *imgs = [@[
                              @"header.png",@"header.png",@"face.png",@"face.png",@"face.png",@"face.png"
                              ]  mutableCopy];

    
    NSMutableArray *res = [NSMutableArray new];
    
    for (int i = 0 ; i < [imgs count] ; i++){
        UIImage *img = [UIImage imageNamed:[imgs objectAtIndex:i]];
        [res addObject:img];
    }
    self.fileNames = res;
    
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    // NSUserDefaultsで保存したURLから画像を作成
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    NSArray *savedImages = (NSMutableArray *)[ud objectForKey:@"images"];
    [ud setObject:nil forKey:@"images"];
    [ud synchronize];

    for(int i = 0 ; i < [savedImages count] ; i++){
        // assets libraryのパスが表示されるのでそのURLから、UIImageを作成する
        NSString *path = [savedImages objectAtIndex:i];
        if ([path length] > 10){
            [library assetForURL:[NSURL URLWithString:path]
                     resultBlock:^(ALAsset *asset) {
                         
                         //画像があればYES、無ければNOを返す
                         if(asset){
                             NSLog(@"データがあります");
                             //ALAssetRepresentationクラスのインスタンスの作成
                             ALAssetRepresentation *assetRepresentation = [asset defaultRepresentation];
                             UIImage *savedImg = [UIImage imageWithCGImage:[assetRepresentation fullScreenImage]];
                             [self.fileNames addObject:savedImg];
                         }else{
                             NSLog(@"データがありません");
                         }
                     } failureBlock: nil];

        }
    }
}



@end
